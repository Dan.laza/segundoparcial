using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiodeCamaras : MonoBehaviour
{
    public Transform primeraPersona, terceraPersona;

        private bool vista;
    void Update()
    {
        if(Input.GetAxis("Mouse ScrollWheel")> 0)
        {
            vista = true;
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            vista = false;
        }

        if(vista==true)
        {
            transform.position = Vector3.Lerp(transform.position, primeraPersona.position, 5 * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, terceraPersona.position, 5 * Time.deltaTime);
        }
    }
}
