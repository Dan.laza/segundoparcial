using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaMov : MonoBehaviour
{
    public Transform puntoA;
    public Transform puntoB;
    public float velocidad = 1f;

    private Vector3 destino;

    void Start()
    {
        
        destino = puntoA.position;
    }

    void Update()
    {
        
        transform.position = Vector3.MoveTowards(transform.position, destino, velocidad * Time.deltaTime);

        
        if (transform.position == destino)
        {
            destino = (destino == puntoA.position) ? puntoB.position : puntoA.position;
        }
    }
}

